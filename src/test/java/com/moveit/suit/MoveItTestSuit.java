package com.moveit.suit;

import com.moveit.cases.LoginTest;
import com.moveit.cases.RequestFileTest;
import com.moveit.cases.SendPackageTest;
import com.moveit.cases.UploadFileTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses({LoginTest.class, RequestFileTest.class, SendPackageTest.class, UploadFileTest.class})
@RunWith(Suite.class)
public class MoveItTestSuit {
}
