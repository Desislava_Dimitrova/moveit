package com.moveit.cases;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;
import java.util.Properties;

public class BaseTest {
    protected WebDriver driver;
    private final Properties configProperties = new Properties();

    BaseTest() {
        try {
            configProperties.load(BaseTest.class.getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            System.out.println("Error loading config properties file");
            e.printStackTrace();
        }
    }

    protected String getProperty(String key) {
        return configProperties.getProperty(key);
    }

    protected void login(WebDriver driver) {
        driver.get(configProperties.getProperty("baseURL"));
        driver.findElement(By.id("form_username")).sendKeys(configProperties.getProperty("userName"));
        driver.findElement(By.id("form_password")).sendKeys(configProperties.getProperty("userPassword"));
        driver.findElement(By.id("submit_button")).click();
    }

    @Before
    public void tearUp() {
        if (getProperty("browser").equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
        } else if (getProperty("browser").equalsIgnoreCase("chrome")) {
            driver = new ChromeDriver();
        }else if (getProperty("browser").equalsIgnoreCase("edge")){
            driver = new EdgeDriver();
        }
    }

    @After
    public void tearDown() {
        driver.close();
    }
}
