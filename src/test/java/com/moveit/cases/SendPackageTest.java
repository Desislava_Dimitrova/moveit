package com.moveit.cases;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;

public class SendPackageTest extends BaseTest {
    File fileToUpload = null;

    @Before
    public void createFile() {
        try {
            fileToUpload = File.createTempFile("" + System.currentTimeMillis(), "_test.txt");
            Files.write(Paths.get(fileToUpload.getAbsolutePath()), "Test".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void deleteFile() {
        try {
            fileToUpload.delete();
        } catch (Exception e) {}
    }

    @Test
    public void sendPackageWithAttachment() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        login(driver);

        driver.findElement(By.xpath("/html/body/table/tbody/tr[3]/td/table/tbody/tr/td[1]/table/tbody/tr[1]/td/div[3]/a/span")).click();
        driver.findElement(By.cssSelector("a[class*='btn-primary']")).click();
        driver.findElement(By.id("tofield")).sendKeys(getProperty("recipient"));
        driver.findElement(By.id("fieldsubject")).sendKeys("subject");
        driver.findElement(By.id("arg04")).sendKeys("notes");

        driver.findElement(By.id("showUploadFilesLink")).click();
        WebElement upload = driver.findElement(By.className("file-selector-input"));
        upload.sendKeys(fileToUpload.getAbsolutePath());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ipswitch_waitindicator")));

        driver.findElement(By.xpath("//*[text()='Upload']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Close']")));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Close']")));

        driver.findElement(By.xpath("//*[text()='Close']")).click();
        driver.findElement(By.id("send-button")).click();

        Assert.assertTrue(driver.findElement(By.id("primaryStatusContent")).getText().contains("Sent package with ID"));
    }

    @Test
    public void unableToSendPackageWithEmptyNotes(){
        login(driver);

        driver.findElement(By.xpath("/html/body/table/tbody/tr[3]/td/table/tbody/tr/td[1]/table/tbody/tr[1]/td/div[3]/a/span")).click();
        driver.findElement(By.cssSelector("a[class*='btn-primary']")).click();
        driver.findElement(By.id("tofield")).sendKeys(getProperty("recipient"));
        driver.findElement(By.id("fieldsubject")).sendKeys("subject");
        driver.findElement(By.id("send-button")).click();

        Assert.assertTrue(driver.findElement(By.id("primaryStatusContent")).getText().contains("The Note field cannot be blank."));
    }

}
