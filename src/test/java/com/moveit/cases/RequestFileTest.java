package com.moveit.cases;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class RequestFileTest extends BaseTest {

    @Test
    public void requestFileWithConfidentialClassificationTest() {
        login(driver);

        driver.findElement(By.cssSelector("div.action.request")).click();
        driver.findElement(By.id("tofield")).sendKeys(getProperty("recipient"));
        driver.findElement(By.id("arg04")).sendKeys(getProperty("notes"));
        WebElement selectDropdown = driver.findElement(By.id("classificationtype"));
        Select dropdown = new Select(selectDropdown);
        dropdown.selectByVisibleText("Confidential");
        driver.findElement(By.id("send-button")).click();

        Assert.assertTrue(driver.findElement(By.id("primaryStatusContent")).getText().contains("Sent package with ID"));
    }
}
