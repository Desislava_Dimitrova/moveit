package com.moveit.cases;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;

public class UploadFileTest extends BaseTest {
    File fileToUpload = null;

    @Before
    public void createFile() {
        try {
            fileToUpload = File.createTempFile("" + System.currentTimeMillis(), "_test.txt");
            Files.write(Paths.get(fileToUpload.getAbsolutePath()), "Test".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void deleteFile() {
        try {
            fileToUpload.delete();
        } catch (Exception e) {}
    }

    @Test
    public void uploadFileTest() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        login(driver);

        driver.findElement(By.id("uploadButton")).click();
        driver.findElement(By.className("wizard-notes-input")).sendKeys("Upload File");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("file-selector-browse")));

        WebElement upload = driver.findElement(By.className("file-selector-input"));
        upload.sendKeys(fileToUpload.getAbsolutePath());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ipswitch_waitindicator")));

        driver.findElement(By.cssSelector(".btn-primary")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[class*='status-complete']")));

        List<WebElement> uploadedFiles = driver.findElements(By.cssSelector("div[title*='" + fileToUpload.getName() + "']"));
        Assert.assertEquals(1, uploadedFiles.size());
    }
}
