package com.moveit.cases;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class LoginTest extends BaseTest {

    @Test
    public void loginTest() {
        login(driver);

        Assert.assertEquals("SIGN OUT", driver.findElement(By.id("lnk_signout")).getText());
    }


}
