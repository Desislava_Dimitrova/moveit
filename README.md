## Overview

The tests in the project cover main functionalities of MOVEit web application. Each scenario is independent and can be 
executed individually or in test suit.

## Prerequisites

1. Java 11 or later
2. Maven
3. Web browser with corresponding webdriver

## Configuration

Update `config.properties` located in <project root>/src/main/resources folder and set correct values for the properties there.
For browser by default is Firefox.

## Running the test suite

Open command prompt or terminal, navigate to project directory and execute the following command:
````
>mvn clean test -Dtest=MoveItTestSuit
````

## Running the suite with surefire report

SureFire may generate a report for all executed tests. 
To generate such report open command prompt or terminal, navigate to project directory and execute the following command:
````
>mvn surefire-report:report
````
A report will be generated with the executed results in the <project root>/target with name surefire-report.
